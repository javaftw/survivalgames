#!/bin/bash

if [ "$#" -ne 3 ]; then
    echo "Error. Usage: make-dirs.sh <prefix> <count> <starting port>"
    echo "Sample: make-dirs.sh SG 10 5000"
    echo "This would make 10 directories with the format SG_N whose ports are 5000, 5001, etc."
    exit 0
fi

prefix=$1
count=$2
port=$3


echo "Making $count directories with the name ${prefix-Server}_N ..."

for ((i=1; i <= $count ; i++))
do

  ((port++))
  name="${prefix-Server}_$i"
  if [[ ${#i} == 1 ]]; then
    name="${prefix-Server}_0$i"
  fi
  echo $name
  cp -R ../ServerTemplate/ $name

  cd $name
  rm server.properties
  printf "#Minecraft server properties
resource-pack-hash=
allow-flight=false
server-port=$port
force-gamemode=false
server-ip=
spawn-npcs=true
white-list=false
spawn-animals=true
online-mode=true
resource-pack=
pvp=true
difficulty=1
gamemode=0
player-idle-timeout=0
max-players=100
generate-structures=true
view-distance=10
motd=$name" > server.properties
  chmod +x start.sh
  cd ..

done

echo "Done."
