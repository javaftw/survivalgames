package com.junglemc.sg.events;

/**
 * ${PROJECT_NAME} by Tarun Boddupalli
 *
 * The MIT License (MIT)
 *
 * Copyright (c) ${YEAR} Tarun Boddupalli
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import com.junglemc.sg.GameState;
import com.junglemc.sg.settings.Utils;
import com.junglemc.sg.sql.SQLUtils;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.*;
import org.bukkit.event.world.ChunkLoadEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.Arrays;
import java.util.List;

public class EventListener implements Listener {

    private List<Material> breakable = Arrays.asList(
            Material.LEAVES,
            Material.LEAVES_2,
            Material.LONG_GRASS,
            Material.YELLOW_FLOWER,
            Material.RED_ROSE,
            Material.VINE,
            Material.WEB,
            Material.TNT
    );

    @EventHandler
    public void onLogin(PlayerLoginEvent e) {
        if (Utils.instance().getData().getGameState() == GameState.STARTING) {
            e.disallow(PlayerLoginEvent.Result.KICK_FULL, "§c§lThe game is full! Try again later.");
        }
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        if (Utils.instance().getData().getGameState() == GameState.LOBBY) {
            e.getPlayer().getInventory().clear();
            Utils.instance().getData().addPlayer(e.getPlayer().getUniqueId());
            e.getPlayer().teleport(new Location(Bukkit.getWorld("lobby"), 0, 128, 0));
        } else if (Utils.instance().getData().getGameState() == GameState.INGAME) {
            Utils.setSpectator(e.getPlayer(), null);
        }
        Utils.healPlayer(e.getPlayer());
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent e) {
        Utils.instance().getData().removePlayer(e.getPlayer().getUniqueId());
    }

    @EventHandler
    public void onBlockBreak(BlockBreakEvent e) {

        if (Utils.isSpectator(e.getPlayer())) {
            e.setCancelled(true);
            return;
        }

        if (Utils.instance().getData().getGameState() == GameState.LOBBY || Utils.instance().getData().getGameState() == GameState.STARTING) {
            e.setCancelled(true);
        } else if (Utils.instance().getData().getGameState() == GameState.INGAME) {
            if (!breakable.contains(e.getBlock().getType())) {
                e.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent e) {
        e.setCancelled(true);

        if (e.getBlock().getType() == Material.TNT) {
            ItemStack subtracted = new ItemStack(e.getPlayer().getItemInHand());
            subtracted.setAmount(subtracted.getAmount() - 1);
            e.getPlayer().setItemInHand(subtracted);
            TNTPrimed tnt = (TNTPrimed) e.getPlayer().getWorld().spawnEntity(e.getBlock().getLocation(), EntityType.PRIMED_TNT);
            tnt.setFuseTicks(20);
        }
    }

    @EventHandler
    public void onMove(PlayerMoveEvent e) {
        if (Utils.instance().getData().getGameState() == GameState.STARTING) {
            if (e.getFrom().getX() == e.getTo().getX()) {
                return;
            }
            if (e.getFrom().getZ() == e.getTo().getZ()) {
                return;
            }
            e.setTo(e.getFrom());
            Utils.playSound(e.getPlayer(), Sound.NOTE_BASS, 1, 0);
        }
    }

    @EventHandler
    public void onDamage(EntityDamageEvent e) {
        if (e.getEntityType() == EntityType.PLAYER) {
            if (Utils.instance().getData().getGameState() == GameState.LOBBY || Utils.instance().getData().getGameState() == GameState.STARTING) {
                e.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void oneEntityDamage(EntityDamageByEntityEvent e) {
        if (Utils.instance().getData().getGameState() == GameState.INGAME) {
            if (e.getEntityType() == EntityType.PLAYER && e.getDamager() instanceof Player) {
                Player damager = (Player) e.getDamager();
                if (Utils.isSpectator(damager)) {
                    e.setCancelled(true);
                }
            }
        }
    }

    @EventHandler
    public void onDeath(PlayerDeathEvent e) {
        if (Utils.instance().getData().getGameState() == GameState.INGAME) {
            String deathMessage = e.getEntity().getName() + " died.";
            EntityDamageEvent.DamageCause cause = e.getEntity().getLastDamageCause().getCause();
            String name = e.getEntity().getName();
            if (cause == EntityDamageEvent.DamageCause.FALL) {
                int distance = (int) Math.ceil(e.getEntity().getFallDistance());
                deathMessage = name + " attacked the ground (Fell " + distance + " blocks).";
            } else if (cause == EntityDamageEvent.DamageCause.DROWNING) {
                deathMessage = name + " thought they could breath underwater.";
            } else if (cause == EntityDamageEvent.DamageCause.SUICIDE) {
                deathMessage = name + " killed themselves.";
            } else if (cause == EntityDamageEvent.DamageCause.ENTITY_EXPLOSION || cause == EntityDamageEvent.DamageCause.BLOCK_EXPLOSION) {
                deathMessage = name + " had their insides blown out.";
            } else if (cause == EntityDamageEvent.DamageCause.SUFFOCATION) {
                deathMessage = name + " tried to breath inside a block.";
            } else if (cause == EntityDamageEvent.DamageCause.STARVATION) {
                deathMessage = name + " died a slow death due to lack of food.";
            } else if (cause == EntityDamageEvent.DamageCause.CUSTOM) {
                deathMessage = name + " tried to go past the world border.";
            } else if (cause == EntityDamageEvent.DamageCause.FALLING_BLOCK) {
                deathMessage = name + " was flattened.";
            } else if (cause == EntityDamageEvent.DamageCause.FIRE || cause == EntityDamageEvent.DamageCause.FIRE_TICK || cause == EntityDamageEvent.DamageCause.LAVA) {
                deathMessage = name + " became a human torch.";
            } else if (cause == EntityDamageEvent.DamageCause.SUICIDE) {
                deathMessage = name + " killed themselves.";
            } else if (cause == EntityDamageEvent.DamageCause.LIGHTNING) {
                deathMessage = name + " was struck by a not-so-nice lightning bolt.";
            } else if (cause == EntityDamageEvent.DamageCause.MAGIC) {
                deathMessage = name + " was magicked.";
            } else if (cause == EntityDamageEvent.DamageCause.POISON) {
                deathMessage = name + " was poisoned.";
            } else if (cause == EntityDamageEvent.DamageCause.CONTACT) {
                deathMessage = name + " tried to hug a cactus.";
            } else {
                if ((e.getEntity().getLastDamageCause() instanceof EntityDamageByEntityEvent)) {
                    EntityDamageByEntityEvent damageEvent = (EntityDamageByEntityEvent) e.getEntity().getLastDamageCause();
                    Player killer = null;
                    if ((damageEvent.getDamager() instanceof Arrow)) {
                        Arrow arrow = (Arrow) damageEvent.getDamager();
                        if ((arrow.getShooter() instanceof Player)) {
                            killer = (Player) arrow.getShooter();
                            deathMessage = name + " thought they could dodge " + killer.getName() + "'s arrows.";
                        }
                    } else if (damageEvent.getDamager() instanceof Player) {
                        killer = (Player) damageEvent.getDamager();
                        deathMessage = Utils.getRandomDeathMessage().replaceAll("%player%", name).replaceAll("%killer%", killer.getName());
                    }
                    if (!Utils.DEBUG) {
                        SQLUtils.addDeath(e.getEntity().getUniqueId());
                    }
                    if (killer != null) {
                        if (!Utils.DEBUG) {
                            SQLUtils.addKill(killer.getUniqueId());
                        }
                        Utils.setSpectator(e.getEntity(), killer);
                    }
                }
            }
            Utils.instance().getData().getPlayers().remove(e.getEntity().getUniqueId());
            Bukkit.broadcastMessage("§c" + Utils.instance().getData().getPlayers().size() + " players remain.");
            e.setDeathMessage("§c" + deathMessage);
            Utils.getGameWorld().strikeLightningEffect(e.getEntity().getLocation());
            Utils.playSoundToAll(Sound.WOLF_HOWL, 1, 2);
            Utils.setSpectator(e.getEntity(), null);
        }
    }

    @EventHandler
    public void onHunger(FoodLevelChangeEvent e) {
        if (Utils.instance().getData().getGameState() == GameState.LOBBY) {
            ((Player) e.getEntity()).setSaturation(8f);
            ((Player) e.getEntity()).setFoodLevel(20);
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent e) {
        if (Utils.instance().getData().getGameState() == GameState.INGAME) {
            if (Utils.isSpectator(e.getPlayer())) {
                e.setCancelled(true);
                if (e.getAction() == Action.RIGHT_CLICK_AIR) {
                    if (e.getItem() != null) {
                        if (e.getItem().getType() == Material.COMPASS) {
                            Utils.showSpectatorGUI(e.getPlayer());
                        }
                    }
                }
            } else {
                if (e.getItem() != null && e.getItem().getType() == Material.WORKBENCH) {
                    e.getPlayer().openInventory(Bukkit.createInventory(null, InventoryType.CRAFTING));
                }
                if (e.getAction() == Action.RIGHT_CLICK_BLOCK) {
                    if (e.getClickedBlock().getType() == Material.CHEST) {
                        if (!Utils.DEBUG)
                            SQLUtils.addChest(e.getPlayer().getUniqueId());
                    }
                }
            }
        } else {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onInventoryClick(InventoryClickEvent e) {
        Player player = (Player) e.getWhoClicked();
        if (e.getCurrentItem() != null) {
            ItemStack item = e.getCurrentItem();
            if (Utils.isSpectator(player)) {
                e.setCancelled(true);
                if (e.getInventory().getTitle().equals("§1Spectate")) {
                    SkullMeta meta = (SkullMeta) item.getItemMeta();
                    e.getWhoClicked().teleport(Bukkit.getPlayer(meta.getOwner()));
                    e.getWhoClicked().closeInventory();
                }
            }
        }
    }

    @EventHandler
    public void onChunkLoad(ChunkLoadEvent e) {
        if (e.getWorld().getName().equals(Utils.GAME_WORLD_NAME)) {
            Utils.fillChests(e.getChunk());
        }
    }
}
