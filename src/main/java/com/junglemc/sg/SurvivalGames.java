package com.junglemc.sg;

import com.junglemc.sg.events.EventListener;
import com.junglemc.sg.settings.Utils;
import com.junglemc.sg.timers.GameTimer;
import com.junglemc.sg.timers.LobbyTimer;
import com.junglemc.sg.timers.PreGameTimer;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.plugin.messaging.PluginMessageListener;

import java.io.ByteArrayInputStream;

/**
 * ${PROJECT_NAME} by Tarun Boddupalli
 *
 * The MIT License (MIT)
 *
 * Copyright (c) ${YEAR} Tarun Boddupalli
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

public class SurvivalGames extends JavaPlugin {

    private static SurvivalGames instance;

    private Data data;

    private LobbyTimer lobbyTimer;
    private GameTimer gameTimer;
    private PreGameTimer preGameTimer;

    @Override
    public void onEnable() {
        instance = this;
        if (Utils.DISABLE_PLUGINS) {
            for (Plugin plugin : getServer().getPluginManager().getPlugins()) {
                if (!plugin.getName().equals(getName())) {
                    getServer().getPluginManager().disablePlugin(plugin);
                }
            }
        }
        getServer().getPluginManager().registerEvents(new EventListener(), this);
        getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
        init();
    }

    public void init() {
        data = new Data();
        lobbyTimer = new LobbyTimer();
        lobbyTimer.start();
        gameTimer = new GameTimer();
        preGameTimer = new PreGameTimer();
    }

    public LobbyTimer getLobbyTimer() {
        return lobbyTimer;
    }

    public GameTimer getGameTimer() {
        return gameTimer;
    }

    public PreGameTimer getPreGameTimer() {
        return preGameTimer;
    }

    public Data getData() {
        return data;
    }

    public static SurvivalGames getInstance() {
        return instance;
    }

}
