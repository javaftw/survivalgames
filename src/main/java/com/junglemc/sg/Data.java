package com.junglemc.sg;

import com.junglemc.sg.commands.VoteCommand;
import com.junglemc.sg.maps.MapUtils;
import com.junglemc.sg.settings.Utils;
import com.junglemc.sg.sql.SQLUtils;
import org.apache.commons.io.FileUtils;
import org.bukkit.Bukkit;
import org.bukkit.WorldCreator;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * ${PROJECT_NAME} by Tarun Boddupalli
 * <p>
 * The MIT License (MIT)
 * <p>
 * Copyright (c) ${YEAR} Tarun Boddupalli
 * <p>
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * <p>
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * <p>
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

public class Data {
    private final List<UUID> players;
    private final List<UUID> spectators;
    private final File lobbyDir;
    private GameState gameState;

    public Data() {
        players = new ArrayList<>();
        spectators = new ArrayList<>();
        lobbyDir = new File("/home/Servers/SurvivalGames/SG-LOBBY");
        gameState = GameState.LOBBY;

        registerCommands();
        addMaps();
        loadLobby();
    }

    public List<UUID> getPlayers() {
        return players;
    }

    public void addPlayer(UUID id) {
        getPlayers().add(id);
    }

    public boolean removePlayer(UUID id) {
        return getPlayers().remove(id);
    }

    public List<UUID> getSpectators() { return spectators; }

    public boolean canStartGame() {
        return Bukkit.getOnlinePlayers().size() >= 2;
    }

    public void loadLobby() {
        try {
            if (!new File("lobby").exists()) {
                FileUtils.copyDirectory(lobbyDir, new File("lobby"));
            }
            WorldCreator.name("lobby").createWorld();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void registerCommands() {
        Utils.instance().getCommand("vote").setExecutor(new VoteCommand());
    }

    public void addMaps() {
        MapUtils.getMaps().clear();
        MapUtils.getMaps().addAll(SQLUtils.loadMaps());
    }

    public GameState getGameState() {
        return gameState;
    }

    public void setGameState(GameState gameState) {
        this.gameState = gameState;
    }
}
