package com.junglemc.sg.timers;

import com.junglemc.sg.GameState;
import com.junglemc.sg.SurvivalGames;
import com.junglemc.sg.maps.MapUtils;
import com.junglemc.sg.settings.Utils;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * SurvivalGames by Tarun Boddupalli
 * <p>
 * The MIT License (MIT)
 * <p>
 * Copyright (c) 2015 Tarun Boddupalli
 * <p>
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * <p>
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * <p>
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
public class GameTimer extends Timer {
    @Override
    public void start() {
        Utils.instance().getData().setGameState(GameState.INGAME);
        setCount(30 * 60); // half an hour
        runTaskTimer(Utils.instance(), 0, 20);
    }

    @Override
    public void run() {
        if (count > 0) {
            if (count % (5 * 60) == 0) {
                Bukkit.broadcastMessage("§6§l>> §e" + formatTime(false) + " left until §cdeathmatch§e.");
            }
            if (Utils.instance().getData().getPlayers().size() == 1) {
                cancel();
                Utils.end(Utils.getPlayersFromUUIDs());
            }
        } else {
            cancel();
        }
        count--;
    }
}
