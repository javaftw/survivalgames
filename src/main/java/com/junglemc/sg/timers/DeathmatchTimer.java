package com.junglemc.sg.timers;

import com.junglemc.sg.GameState;
import com.junglemc.sg.maps.MapUtils;
import com.junglemc.sg.settings.Utils;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * SurvivalGames by Tarun Boddupalli
 * <p>
 * The MIT License (MIT)
 * <p>
 * Copyright (c) 2015 Tarun Boddupalli
 * <p>
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * <p>
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * <p>
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
public class DeathmatchTimer extends Timer {
    @Override
    public void start() {
        Utils.instance().getData().setGameState(GameState.DEATHMATCH);
        Utils.fillAllChests();
        List<Location> locations = MapUtils.getMapWithMostVotes().getLocations();
        List<Player> players = Utils.getPlayersFromUUIDs();
        for (int i = 0; i < players.size(); i++) {
            Player player = players.get(i);
            Location loc = locations.get(i);
            player.teleport(loc);
        }
        Bukkit.broadcastMessage("§4§l>> §cDeathmatch has started! Fight to the death!");
        setCount(300);
        runTaskTimer(Utils.instance(), 0, 20);
    }

    @Override
    public void run() {
        if (count > 0) {
            if (count > 30) {
                if (count % 60 == 0) {
                    Bukkit.broadcastMessage("§b§l>> §a" + formatTime(false) + " until deathmatch ends.");
                }
            } else {
                if (count > 5) {
                    if (count % 5 == 0) {
                        Bukkit.broadcastMessage("§b§l>> §a" + formatTime(false) + " until deathmatch ends.");
                    }
                } else {
                    Bukkit.broadcastMessage("§b§l>> §aDeathmatch ends in " + count + " seconds.");
                }
            }

            if (Utils.instance().getData().getPlayers().size() == 1) {
                cancel();
                Utils.end(Utils.getPlayersFromUUIDs());
            }
        } else {
            cancel();
            Utils.end(Utils.getPlayersFromUUIDs());
        }
        count--;
    }
}
