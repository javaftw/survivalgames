package com.junglemc.sg.timers;

import com.junglemc.sg.maps.Map;
import com.junglemc.sg.maps.MapUtils;
import com.junglemc.sg.settings.Utils;
import org.apache.commons.lang3.Validate;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

/*

The MIT License (MIT)

Copyright (c) 2014 Tarun Boddupalli

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

*/

public class LobbyTimer extends Timer {


    @Override
    public void start() {
        setCount(Utils.DEBUG ? Utils.DEBUG_TIME : Utils.NORMAL_TIME);
        runTaskTimer(Utils.instance(), 0, 20);
    }

    @Override
    public void run() {
        if (count > 0) {
            if (count > 10) {
                if (count % 30 == 0) {
                    Bukkit.broadcastMessage("§9§l>> §6" + formatTime(false) + " §7left until lobby ends.");
                    Utils.playSoundToAll(Sound.ANVIL_LAND, 1, 2);
                    Bukkit.getOnlinePlayers().stream().filter(p -> MapUtils.getPlayerMapChoice(p.getUniqueId()) == null).forEach(p -> p.sendMessage("§9§l>> §7Vote for a map using /vote!"));
                }

            } else {
                Bukkit.broadcastMessage("§6§l>> §bGame starting in " + getCount() + "...");
                Utils.playSoundToAll(Sound.WOOD_CLICK, 1, 1);
            }
            decrement();
        } else {
            if (Utils.instance().getData().canStartGame()) {
                cancel();
                Map map = MapUtils.getMapWithMostVotes();
                MapUtils.loadSelectedMap();
                Bukkit.getScheduler().scheduleSyncDelayedTask(Utils.instance(), () -> {
                    List<Location> locations = map.getLocations();
                    Utils.setLocations(locations);
                    List<Player> players = new ArrayList<>(Bukkit.getOnlinePlayers());
                    locations.forEach(l -> l.setWorld(Utils.getGameWorld()));
                    for (int i = 0; i < players.size(); i++) {
                        Player player = players.get(i);
                        Location loc = locations.get(i);
                        player.teleport(loc);
                    }
                    Utils.instance().getPreGameTimer().start();
                }, 60);

            } else {
                reset();
            }
        }
    }

    public void reset() {
        Utils.playSoundToAll(Sound.ANVIL_LAND, 1, 0.1f);
        setCount(Utils.DEBUG ? Utils.DEBUG_TIME : Utils.NORMAL_TIME);
        Bukkit.broadcastMessage("§c§lTimer reset. Not enough players.");
    }


    // TODO get msgs from SQL
}
