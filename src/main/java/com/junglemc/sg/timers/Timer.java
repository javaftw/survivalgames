package com.junglemc.sg.timers;

import org.bukkit.scheduler.BukkitRunnable;

/**
 * Created by family on 12/8/2014.
 */

/*

The MIT License (MIT)

Copyright (c) 2014 Tarun Boddupalli

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

*/

public abstract class Timer extends BukkitRunnable {
    protected int count = 0;

    public abstract void start();

    public String formatTime(boolean hour) {
        int currentTime = count;
        if (hour) {
            int hours = currentTime / 3600;
            int rem = currentTime % 3600;
            int mins = rem / 60;
            int secs = rem % 60;
            if (hours > 0) {
                return hours + "h" + mins + "m" + secs + "s";
            } else if (mins > 0) {
                return mins + "m" + secs + "s";
            }
            return secs + "s";
        }
        int mins = currentTime / 60;
        int rem = currentTime % 60;
        int secs = rem % 60;
        return mins + "m" + secs + "s";
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public void decrement() {
        count--;
    }
}
