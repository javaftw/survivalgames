package com.junglemc.sg.timers;

import com.junglemc.sg.GameState;
import com.junglemc.sg.SurvivalGames;
import com.junglemc.sg.settings.Utils;
import org.bukkit.Bukkit;
import org.bukkit.Sound;

import javax.rmi.CORBA.Util;
import java.util.Arrays;

/**
 * SurvivalGames by Tarun Boddupalli
 * <p>
 * The MIT License (MIT)
 * <p>
 * Copyright (c) 2015 Tarun Boddupalli
 * <p>
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * <p>
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * <p>
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
public class PreGameTimer extends Timer {

    @Override
    public void start() { // TODO implement any necessary things to do here
        Utils.getGameWorld().setTime(1000);
        Utils.instance().getData().setGameState(GameState.STARTING);
        Utils.fillAllChests();
        setCount(60);
        runTaskTimer(Utils.instance(), 0, 20);
    }

    @Override
    public void run() {
        if (count > 0) {
            if (count > 24) {
                if (count % 10 == 0) {
                    Bukkit.broadcastMessage("§b§l>> §aGame starting in " + count + " seconds.");
                    Utils.playSoundToAll(Sound.ANVIL_LAND, 1, 2f);
                }
            } else {
                Bukkit.broadcastMessage("§b§l>> §a§l" + count + ".");
                Utils.playSoundToAll(Sound.AMBIENCE_THUNDER, 1, 1);
                Utils.getGameWorld().strikeLightningEffect(Utils.getLocations().get(count - 1));
            }
        } else {
            Bukkit.broadcastMessage("§b§l>> The game has started!");
            Utils.playSoundToAll(Sound.AMBIENCE_THUNDER, 1, 2);
            cancel();
            Utils.instance().getGameTimer().start();
        }
        count--;
    }
}
