package com.junglemc.sg.maps;

import com.junglemc.sg.settings.Utils;
import org.apache.commons.io.FileUtils;
import org.bukkit.Bukkit;
import org.bukkit.WorldCreator;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

/**
 * ${PROJECT_NAME} by Tarun Boddupalli
 * <p>
 * The MIT License (MIT)
 * <p>
 * Copyright (c) ${YEAR} Tarun Boddupalli
 * <p>
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * <p>
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * <p>
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

public class MapUtils {

    private static final List<Map> maps = new ArrayList<>();

    private static final HashMap<UUID, Map> mapsVotedFor = new HashMap<>();

    public static final File MAPS_DIR = new File("/home/Servers/SurvivalGames/SG-MAPS");

    public static List<Map> getMaps() {
        return maps;
    }

    public static Map getMapById(int id) {
        for (Map map : maps) {
            if (map.getId() == id) return map;
        }
        return null;
    }

    public static Map getMapWithMostVotes() {
        Map max = maps.get(0);
        for (Map m : maps) {
            if (max.getVotes() < m.getVotes()) max = m;
        }

        return max;
    }

    public static Map getPlayerMapChoice(UUID id) {
        return mapsVotedFor.get(id);
    }

    public static void setPlayerMapChoice(UUID id, Map map) {
        map.addVote();
        mapsVotedFor.put(id, map);
        Bukkit.getPlayer(id).sendMessage("§a>> §bCast your vote to " + map.getName() + "!");
    }

    public static void loadSelectedMap() {
        Map map = getMapWithMostVotes();
        Bukkit.broadcastMessage("§8§l>> §7§oLoading map " + map.getName() + "...");
        try {
            new File(Utils.GAME_WORLD_NAME).delete(); //delete the old one
            FileUtils.copyDirectory(map.getDir(), new File(Utils.GAME_WORLD_NAME)); // copy the new one
            WorldCreator.name(Utils.GAME_WORLD_NAME).createWorld(); // load the world
        } catch (IOException e) {
            e.printStackTrace();
        }
        Bukkit.broadcastMessage("§8§l>> §7§oDone. Teleporting...");

    }
}
