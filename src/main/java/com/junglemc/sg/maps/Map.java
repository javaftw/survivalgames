package com.junglemc.sg.maps;

import org.bukkit.Location;
import org.bukkit.util.FileUtil;

import java.io.File;
import java.util.List;

/*

The MIT License (MIT)

Copyright (c) 2014 Tarun Boddupalli

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

*/

public class Map {

    private File dir;
    private String creator;
    private String name;
    private int id;
    private List<Location> locations;
    private int votes;

    public Map(File dir, int id, String name, String creator, List<Location> locations) {
        this.dir = dir;
        this.id = id;
        this.name = name;
        this.creator = creator;
        this.locations = locations;
    }

    public File getDir() {
        return dir;
    }

    public String getInfo() {
        return "§b " + getName() + "§a by §b" + getCreator() + "§a";
    }

    public String getCreator() {
        return creator;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public int getVotes() {
        return votes;
    }

    public void addVote() {
        votes++;
    }

    public List<Location> getLocations() {
        return locations;
    }

    public void setId(int id) {
        this.id = id;
    }
}
