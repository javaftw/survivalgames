package com.junglemc.sg.commands;

import com.junglemc.sg.GameState;
import com.junglemc.sg.SurvivalGames;
import com.junglemc.sg.maps.Map;
import com.junglemc.sg.maps.MapUtils;
import com.junglemc.sg.settings.Utils;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by family on 12/12/2014.
 */

/*

The MIT License (MIT)

Copyright (c) 2014 Tarun Boddupalli

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

*/

public class VoteCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (Utils.instance().getData().getGameState() != GameState.LOBBY) {
            sender.sendMessage("You cannot vote at this time!");
            return true;
        }
        if (sender instanceof Player) {
            Player player = (Player) sender;
            if (MapUtils.getPlayerMapChoice(player.getUniqueId()) != null) {
                player.sendMessage("§1§l>> §cYou've already voted for §d" + MapUtils.getPlayerMapChoice(player.getUniqueId()).getName() + ".");
                player.sendMessage(MapUtils.getPlayerMapChoice(player.getUniqueId()).getInfo());
            } else {
                if (args.length == 0) {
                    player.sendMessage("§9§l>> §5Maps to vote for:");
                    showMapsInfo(player);
                    player.sendMessage("§5------------------------------------");
                    player.sendMessage("§5To vote for a map, do /vote <Map ID>");
                } else if (args.length == 1) {
                    int id;
                    try {
                        id = Integer.parseInt(args[0]);
                    } catch (NumberFormatException e) {
                        player.sendMessage("§4X §cUsage: /vote <Map ID>");
                        player.sendMessage("§4X §cDo /vote to see maps.");
                        return true;
                    }
                    if (MapUtils.getMapById(id) == null) {
                        player.sendMessage("§4X §cThat map doesn't exist!");
                        player.sendMessage("§5------------------------------------");
                        showMapsInfo(player);
                        player.sendMessage("§5------------------------------------");
                        return true;
                    }
                    MapUtils.setPlayerMapChoice(player.getUniqueId(), MapUtils.getMapById(id));
                }
            }
        }
        return true;
    }

    private void showMapsInfo(Player player) {
        for (Map map : MapUtils.getMaps()) {
            player.sendMessage(" • §d1 - | " + map.getVotes() + " Votes | " + map.getName());
        }
    }
}
