package com.junglemc.sg.settings;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import com.junglemc.sg.GameState;
import com.junglemc.sg.SurvivalGames;
import com.junglemc.sg.sql.SQLUtils;
import org.bukkit.*;
import org.bukkit.block.BlockState;
import org.bukkit.block.Chest;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.plugin.Plugin;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * ${PROJECT_NAME} by Tarun Boddupalli
 * <p>
 * The MIT License (MIT)
 * <p>
 * Copyright (c) ${YEAR} Tarun Boddupalli
 * <p>
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * <p>
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * <p>
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

public class Utils {

    public static final String GAME_WORLD_NAME = "game-world";
    public static final boolean DISABLE_PLUGINS = true;
    public static boolean DEBUG = true;
    public static final int DEBUG_TIME = 30;
    public static final int NORMAL_TIME = 120;
    public static Random rand = new Random();
    private static final ItemStack spectatorCompass;

    static {
        spectatorCompass = new ItemStack(Material.COMPASS);
        ItemMeta meta = Bukkit.getItemFactory().getItemMeta(Material.COMPASS);
        meta.setDisplayName("§bSpectate §7[§8Right Click§7]");
        meta.setLore(Arrays.asList("§9Right click to watch other players!"));
        spectatorCompass.setItemMeta(meta);
    }

    private static List<Location> locations;

    public static SurvivalGames instance() {
        return SurvivalGames.getInstance();
    }

    public static void playSoundToAll(Sound sound, float volume, float pitch) {
        for (Player p : Bukkit.getOnlinePlayers()) {
            playSound(p, sound, volume, pitch);
        }
    }

    public static void playSound(Player p, Sound sound, float volume, float pitch) {
        p.playSound(p.getLocation(), sound, volume, pitch);
    }

    public static void healPlayer(Player p) {
        p.setGameMode(GameMode.SURVIVAL);
        p.setHealth(20);
        p.setFireTicks(0);
        p.getInventory().clear();
        p.getInventory().setArmorContents(null);
        p.getActivePotionEffects().forEach(e -> p.removePotionEffect(e.getType()));
        p.setLevel(0);
    }

    public static World getGameWorld() {
        return Bukkit.getWorld(GAME_WORLD_NAME);
    }

    public static void log(Object msg) {
        instance().getLogger().info(msg.toString());
    }

    public static void fillChests(Chunk chunk) {
        // int trappedChests = 0;
        List<BlockState> chests = Arrays.asList(chunk.getTileEntities()).stream().filter(s -> s instanceof Chest).collect(Collectors.toList());
        for (BlockState chest : chests) {
            if (chest.getType() == Material.CHEST) {
                Inventory inv = ((Chest) chest).getBlockInventory();
                inv.clear();
                for (int i = 0; i < rand.nextInt(8) + 2; i++) {
                    int slot = rand.nextInt(inv.getSize());
                    if (inv.getItem(slot) == null) {
                        inv.setItem(slot, getRandomItem(1));
                    }
                }
            } else if (chest.getType() == Material.TRAPPED_CHEST) {
                // trappedChests++;
                Inventory inv = ((Chest) chest).getBlockInventory();
                inv.clear();
                for (int i = 0; i < rand.nextInt(8) + 2; i++) {
                    int slot = rand.nextInt(inv.getSize());
                    if (inv.getItem(slot) == null) {
                        inv.setItem(slot, getRandomItem(2));
                    }
                }
            }
        }
        // log(trappedChests + " trapped chests were found.");
    }

    public static void fillAllChests() {
        Arrays.asList(getGameWorld().getLoadedChunks()).forEach(Utils::fillChests);
    }

    private static ItemStack getRandomItem(int tier) {
        SQLUtils.setDatabase("stats");
        ResultSet set = SQLUtils.query("SELECT * FROM `sg-items`");
        List<ItemStack> items = new ArrayList<>();
        try {
            while (set.next()) {
                if (set.getInt("tier") == tier) {
                    ItemStack item = new ItemStack(Material.valueOf(set.getString("material")), set.getInt("count"), (short) set.getInt("data"));
                    if (!set.getString("custom_name").equalsIgnoreCase("default")) {
                        ItemMeta meta = item.getItemMeta();
                        meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', set.getString("custom_name")));
                        item.setItemMeta(meta);
                    }
                    int chance = set.getInt("chance");
                    for (int i = 0; i < chance; i++) {
                        items.add(item);
                    }
                }

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return items.get(rand.nextInt(items.size()));
    }

    public static String getRandomDeathMessage() {
        String[] messages = {
                "%player% was pummeled by %killer%",
                "%player% was brutally murdered by %killer%",
                "%killer% anally murdered %player%"
        };
        return messages[rand.nextInt(messages.length)];
    }

    public static List<Player> getPlayersFromUUIDs() {
        return instance().getData().getPlayers().stream().map(Bukkit::getPlayer).collect(Collectors.toList());
    }

    public static void setSpectator(Player player, Player killer) {
        if (!instance().getData().removePlayer(player.getUniqueId())) {
            player.teleport(Bukkit.getWorld(GAME_WORLD_NAME).getSpawnLocation());
        }
        instance().getData().getSpectators().add(player.getUniqueId());
        healPlayer(player);
        player.setGameMode(GameMode.SPECTATOR);
        player.getInventory().setItem(5, spectatorCompass);
        player.sendMessage("§4§l======================================");
        player.sendMessage("§8§o§n>> §7§oYou are a spectator.");
        if (killer != null) {
            player.sendMessage("§4>> §cYour killer (" + killer.getName() + ") had " + (killer.getHealth() / 2) + " hearts.");
        }
        player.sendMessage("§4§l======================================");
    }

    public static boolean isSpectator(Player p) {
        return instance().getData().getSpectators().contains(p.getUniqueId());
    }

    public static void showSpectatorGUI(Player p) {
        Inventory inv = Bukkit.createInventory(null, 27, "§1Spectate");
        for (UUID id : instance().getData().getPlayers()) {
            ItemStack skull = new ItemStack(Material.SKULL_ITEM, 1, (short) 3);
            SkullMeta skullMeta = (SkullMeta) Bukkit.getItemFactory().getItemMeta(Material.SKULL_ITEM);
            skullMeta.setOwner(Bukkit.getPlayer(id).getName());
            skullMeta.setDisplayName("§e" + Bukkit.getPlayer(id).getName());
            skull.setItemMeta(skullMeta);
            inv.addItem(skull);
        }
        p.openInventory(inv);
    }

    public static void end(List<Player> players) {
        if (players.size() == 1) {
            Player winner = players.get(0);
            Bukkit.broadcastMessage("§a§l>> §aWe have a winner!");
            Bukkit.broadcastMessage("§a§l>> §a" + winner.getName() + " won SG!");
            SQLUtils.addWin(winner.getUniqueId());
        } else {
            Bukkit.broadcastMessage("§a§l>> §aWinners:");
            players.forEach(p -> {
                Bukkit.broadcastMessage(" - §a" + p.getName());
                SQLUtils.addWin(p.getUniqueId());
            });
        }
        Bukkit.getScheduler().cancelAllTasks();
        Bukkit.getScheduler().runTaskLater(instance(), () -> {
            Bukkit.getOnlinePlayers().forEach(p -> connectAll(instance(), "LOBBY_1"));
            Bukkit.shutdown();
        }, 200);
        instance().getData().setGameState(GameState.RESTARTING);
    }

    public static void setLocations(List<Location> locations) {
        Utils.locations = locations;
    }

    public static List<Location> getLocations() {
        return locations;
    }

    public static void connectPlayer(Plugin plugin, Player p, String server) {
        ByteArrayDataOutput out = ByteStreams.newDataOutput();
        out.writeUTF("Connect");
        out.writeUTF(server);
        p.sendPluginMessage(plugin, "BungeeCord", out.toByteArray());
    }

    public static void connectAll(Plugin plugin, String server) {
        Bukkit.getOnlinePlayers().forEach(p -> connectPlayer(plugin, p, server));
    }
}
