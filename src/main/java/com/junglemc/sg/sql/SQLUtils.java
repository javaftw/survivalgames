package com.junglemc.sg.sql;

import com.junglemc.sg.maps.Map;
import com.junglemc.sg.maps.MapUtils;
import com.junglemc.sg.settings.Utils;
import org.apache.commons.lang3.SystemUtils;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

/**
 * Created by family on 12/20/2014.
 */

/*

The MIT License (MIT)

Copyright (c) 2014 Tarun Boddupalli

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

*/

public class SQLUtils {

    private static MySQL SQL = new MySQL("localhost", "3306", "stats", "root", "JX26Jungle");

    static {
        if (!SystemUtils.IS_OS_LINUX) {
            SQL = new MySQL("beta.junglemc.com", "3306", "stats", "remote", "JX26Jungle");
        }
    }

    public static void setDatabase(String database) {
        SQL = new MySQL("localhost", "3306", database, "root", "JX26Jungle");
    }

    public static ResultSet query(String sql) {
        try {
            return SQL.open().createStatement().executeQuery(sql);
        } catch (SQLException e) {
            return null;
        }
    }

    public static void update(String sql) {
        try {
            SQL.open().createStatement().executeUpdate(sql);
        } catch (SQLException e) { e.printStackTrace(); }
    }

    public static List<Map> loadMaps() {
        setDatabase("stats");
        List<Map> maps = new ArrayList<>();
        ResultSet set = query("SELECT * FROM `sg-maps`");
        try {
            while (set.next()) {
                int id = set.getInt("id");
                String name = set.getString("name");
                String creator = set.getString("creator");
                String[] spawnPoints = set.getString("spawnpoints").split(";");
                List<Location> locations = new ArrayList<>();
                for (String spawnPoint : spawnPoints) {
                    try {
                        String[] pos = spawnPoint.split(" ");
                        locations.add(new Location(null, Double.parseDouble(pos[0]), Double.parseDouble(pos[1]), Double.parseDouble(pos[2])));
                    } catch (NumberFormatException e) {
                        System.out.println("name = " + name);
                        System.out.println("creator = " + creator);
                        System.out.println("spawnPoints = " + Arrays.toString(spawnPoints));
                        System.out.println("spawnPoint = " + spawnPoint);
                    }
                }
                Map map = new Map(new File(MapUtils.MAPS_DIR, name), id, name, creator, locations);
                maps.add(map);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        List<Map> pickedMaps = new ArrayList<>();

        for (int i = 0; i < 5; i++) {
            Map map = maps.get(Utils.rand.nextInt(maps.size()));
            maps.remove(map);
            map.setId(i+1);
            pickedMaps.add(map);
        }

        return pickedMaps;
    }

    public static void addKill(UUID uuid) {
        try {
            ResultSet set = query("SELECT `kills` FROM sg WHERE uuid='" + uuid.toString() + "'");
            if (set.next()) {
                int kills = set.getInt("kills");
                update("UPDATE `sg` SET kills=" + (kills + 1) + " WHERE uuid='" + uuid.toString() + "'");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void addDeath(UUID uuid) {
        try {
            ResultSet set = query("SELECT `deaths` FROM sg WHERE uuid='" + uuid.toString() + "'");
            if (set.next()) {
                int deaths = set.getInt("deaths");
                update("UPDATE `sg` SET deaths=" + (deaths + 1) + " WHERE uuid='" + uuid.toString() + "'");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void addWin(UUID uuid) {
        try {
            ResultSet set = query("SELECT `wins` FROM sg WHERE uuid='" + uuid.toString() + "'");
            if (set.next()) {
                int wins = set.getInt("wins");
                update("UPDATE `sg` SET wins=" + (wins + 1) + " WHERE uuid='" + uuid.toString() + "'");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void addChest(UUID uuid) {
        try {
            ResultSet set = query("SELECT `chests` FROM sg WHERE uuid='" + uuid.toString() + "'");
            if (set.next()) {
                int chests = set.getInt("chests");
                update("UPDATE `sg` SET chests=" + (chests + 1) + " WHERE uuid='" + uuid.toString() + "'");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}





