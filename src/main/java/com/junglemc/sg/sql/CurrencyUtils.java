package com.junglemc.sg.sql;

import com.junglemc.sg.settings.Utils;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * SurvivalGames by Tarun Boddupalli
 * <p>
 * The MIT License (MIT)
 * <p>
 * Copyright (c) 2015 Tarun Boddupalli
 * <p>
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * <p>
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * <p>
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
public class CurrencyUtils {
    public static int getCredits(String p) {
        SQLUtils.setDatabase("permissions");
        try {
            
            ResultSet res = SQLUtils.query("SELECT * FROM users WHERE username= '" + p + "';");
            if (res.next()) {
                return res.getInt("credits");
            } else {
                return 0;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        } finally {
            SQLUtils.setDatabase("stats");
        }

    }

    public static int permGetPower(String p) {
        SQLUtils.setDatabase("permissions");
        try {
            
            ResultSet res = SQLUtils.query("SELECT * FROM users WHERE name= '" + p + "';");
            if (res.next()) {
                ResultSet res2 = SQLUtils.query("SELECT * FROM ranks WHERE id= '" + res.getInt("rankid") + "';");
                if (res2.next()) {
                    return res2.getInt("power");
                } else {
                    return 0;
                }
            } else {
                return 0;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        } finally {
            SQLUtils.setDatabase("stats");
        }

    }

    public static String permGetDisplayRank(String name) {
        SQLUtils.setDatabase("permissions");
        try {
            
            ResultSet res = SQLUtils.query("SELECT * FROM users WHERE username = '" + name + "';");

            if (res.next()) {
                ResultSet res2 = SQLUtils.query("SELECT * FROM ranks WHERE id = '" + res.getInt("rankid") + "';");
                if (res2.next()) {
                    return res2.getString("display");
                } else {
                    return "User";
                }
            } else {
                return "User";
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return "";
        } finally {
            SQLUtils.setDatabase("stats");
        }
    }

    public static void addCredits(int amount, Player p) {
        SQLUtils.setDatabase("permissions");
        try {

            ResultSet res = SQLUtils.query("SELECT * FROM users WHERE username = '" + p.getName() + "';");
            if (res.next()) {
                p.sendMessage("§8§l[§6TJN§8§l] " + ChatColor.GREEN + "+" + amount + " credits!");
                int ex = res.getInt("credits") + amount;
                Utils.log("[TJNUtils] Added " + amount + " credits to UUID " + res.getString("uuid") + " [" + res.getString("username") + "]");
                SQLUtils.update("UPDATE `users` SET `credits`='" + ex + "' WHERE uuid = '" + p.getName() + "';'");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            SQLUtils.setDatabase("stats");
        }
    }

    public static int getCredits(Player p) {
        try {
            SQLUtils.setDatabase("permissions");
            ResultSet set = SQLUtils.query("SELECT * FROM users WHERE username='" + p.getName() + "';");
            if (set.next()) {
                return set.getInt("credits");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            SQLUtils.setDatabase("stats");
        }
        return -1;
    }
}
